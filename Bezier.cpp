#include "Bezier.h"
#include <iostream>
using namespace std;

void DrawCurve2(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3)
{
    for (float i = 0; i < 1; i+= 0.001)
    {
        int x = (1-i)*(1-i) * p1.x + 2 * i * (1-i) * p2.x + i*i * p3.x;

        int y = (1-i)*(1-i) * p1.y + 2 * i * (1-i) * p2.y + i*i * p3.y;

        SDL_SetRenderDrawColor(ren,255,0,0,255);
        SDL_RenderDrawPoint(ren,x,y);

    }

}
void DrawCurve3(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3, Vector2D p4)
{
    for (float i = 0; i < 1; i+= 0.001)
    {
        int x = (1-i)*(1-i)*(1-i) * p1.x + 3 * i * (1-i) * (1-i) * p2.x + 3 * i *i * (1-i) * p3.x + i*i*i * p4.x;

        int y = (1-i)*(1-i)*(1-i) * p1.y + 3 * i * (1-i) * (1-i) * p2.y + 3 * i *i * (1-i) * p3.y + i*i*i * p4.y;

        SDL_SetRenderDrawColor(ren,255,0,0,255);
        SDL_RenderDrawPoint(ren,x,y);

    }
}



